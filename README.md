# Front End Applicant Project

Recreate the `source/applicant_project.psd` file using only HTML, JavaScript, and CSS. More specific instructions and guidelines are provided below.

1. Fork this repository.
2. The HTML content should go in the `index.html` file provided.
3. All CSS styles should go in the `css/styles.css` file.
4. Elements should scale down appropriately for mobile devices.
5. All JavaScript should go in `js/main.js`
6. On page load, the center logo and three ribbons should animate in, one at a time, from above.
7. We have included jQuery in `js/jquery.js` if you want to use it.
8. Do not use any other jQuery or JavaScript plugins.
9. All images should go in the `img/` directory.
10. Testing / Evaluation:
    - The project should be able to work locally on a device without Internet (self-contained).
    - Use semantic markup and proper coding standards. Code should look as good as it functions.
    - Less is more. Make the project as light-weight and performant as possible.
11. When you are done, send us a link to your repository.
12. ???
13. Profit.
